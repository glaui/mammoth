var _ = require("underscore");


function symmetricMarkdownElement (end) {
  return markdownElement(end, end);
}

function markdownElement (start, end, list) {
  return function () {
    return { start: start, end: end };
  };
}

function markdownLink (attributes) {
  var href = attributes.href || "";
  if (href) {
    return {
      start: "[",
      end: "](" + href + ")",
      anchorPosition: "before"
    };
  } else {
    return {};
  }
}

function markdownImage (attributes) {
  var src = attributes.src || "";
  var altText = attributes.alt || "";
  if (src || altText) {
    return { start: "![" + altText + "](" + src + ")" };
  } else {
    return {};
  }
}

function markdownList (options) {
  return function (attributes, list, listItem) {
    if (list && list.isTd) {
      return {
        start: '',
        end: '',
        list: {
          isTd: list.isTd,
          isOrdered: options.isOrdered,
          indent: list ? list.indent + 1 : 0,
          count: 0
        }
      }
    } else {
      return {
        start: list ? "\n" : "",
        end: list ? "" : "\n",
        list: {
          isOrdered: options.isOrdered,
          indent: list ? list.indent + 1 : 0,
          count: 0
        }
      };
    }
  };
}
let tdLength = 0;
function markdownListItem (tagName) {

  return function (attributes, list, listItem) {
    list = list || { indent: 0, isOrdered: false, count: 0 };
    list.count++;
    listItem.hasClosed = false;
    var bullet = list.isOrdered ? list.count + "." : "-";
    var start = '';
    if (tagName === 'td') {
      start = repeatString("\t", list.indent) + (list.count === 1 ? ' | ' : '')
      if (list.count > tdLength) {
        tdLength = list.count;
      }
    } else if (tagName === 'tr') {
      start = list.count === 2 && tdLength > 0 ? '---' + repeatString(" | ------", tdLength - 1) + '\n' : repeatString("\t", list.indent);
      tdLength = 0;
    } else {
      start = repeatString("\t", list.indent) + bullet + " ";
      tdLength = 0;
    }
    let listo = (function () {
      if (tagName === 'tr') {
        return {
          indent: 0,
          count: 0
        }
      } else if (tagName === 'td') {
        return { ...list, isTd: true }
      } else {
        return null;
      }
    })()
    return {
      start: start,
      end: function () {
        let res = ''
        if (!listItem.hasClosed) {
          listItem.hasClosed = true;
          switch (tagName) {
            case 'li':
              res = list.isTd ? '' : '\n'
              break;
            case 'td':
              res = ' | '
              break;
            default:
              break;
          }

        } else {
          switch (tagName) {
            case 'tr':
              res = '\n'
              break;
            case 'td':
              res = ' | '
              break;
            default:
              break;
          }

        }
        return res;
      },
      list: listo
    };
  }
}
function markdownP (start, end) {
  return function (attr, list, listItem) {
    if (list) {
      return { start: '', end: '' }
    } else {

      return { start, end };
    }
  }
}
var htmlToMarkdown = {
  "p": markdownP("", "\n\n"),
  "br": markdownElement("", "  \n"),
  "ul": markdownList({ isOrdered: false }),
  "ol": markdownList({ isOrdered: true }),
  "li": markdownListItem('li'),
  "strong": symmetricMarkdownElement("__"),
  "em": symmetricMarkdownElement("*"),
  "a": markdownLink,
  "img": markdownImage,
  "td": markdownListItem('td'),
  "tr": markdownListItem('tr'),
  "table": markdownList({ isOrdered: false })
};

(function () {
  for (var i = 1; i <= 6; i++) {
    htmlToMarkdown["h" + i] = markdownElement(repeatString("#", i) + " ", "\n\n");
  }
})();

function repeatString (value, count) {
  return new Array(count + 1).join(value);
}

function markdownWriter () {
  var fragments = [];
  var elementStack = [];
  var list = null;
  var listItem = {};

  function open (tagName, attributes) {
    attributes = attributes || {};

    var createElement = htmlToMarkdown[tagName] || function () {
      return {};
    };
    var element = createElement(attributes, list, listItem);
    elementStack.push({ end: element.end, list: list });

    if (element.list) {
      list = element.list;
    }

    var anchorBeforeStart = element.anchorPosition === "before";
    if (anchorBeforeStart) {
      writeAnchor(attributes);
    }

    fragments.push(element.start || "");
    if (!anchorBeforeStart) {
      writeAnchor(attributes);
    }
  }

  function writeAnchor (attributes) {
    if (attributes.id) {
      fragments.push('<a id="' + attributes.id + '"></a>');
    }
  }

  function close (tagName) {
    var element = elementStack.pop();
    list = element.list;
    var end = _.isFunction(element.end) ? element.end() : element.end;
    fragments.push(end || "");
  }

  function selfClosing (tagName, attributes) {
    open(tagName, attributes);
    close(tagName);
  }

  function text (value) {
    fragments.push(escapeMarkdown(value));
  }

  function asString () {
    return fragments.join("");
  }

  return {
    asString: asString,
    open: open,
    close: close,
    text: text,
    selfClosing: selfClosing
  };
}

exports.writer = markdownWriter;

function escapeMarkdown (value) {
  return value
    .replace(/\\/g, '\\\\')
    .replace(/([\`\*_\{\}\[\]\(\)\#\+\-\.\!])/g, '\\$1');
}
